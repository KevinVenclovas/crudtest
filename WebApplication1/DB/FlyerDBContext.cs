﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Core.Models;

namespace WebApplication1.DB
{
    public class FlyerDBContext: DbContext
    {

        public FlyerDBContext()
        {
        }

        public FlyerDBContext(DbContextOptions<FlyerDBContext> options) : base(options)
        {
        }

        public virtual DbSet<Order> Orders { get; set; } 


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
        }


    }
}

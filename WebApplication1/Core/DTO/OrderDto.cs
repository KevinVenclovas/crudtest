﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Core.DTO
{
    public class OrderDto
    {
        public int Id { get; set; }
        public string OrderName { get; set; }

    }
}

﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Core.Models;
using WebApplication1.DB;

namespace WebApplication1.Core.Services
{
    public class OrdersRepository : IOrderRepository
    {

        FlyerDBContext dbContext;

        public OrdersRepository(FlyerDBContext dbContext)
        {
            this.dbContext = dbContext;
        }


        public async Task<Order> GetOrder(int id)
        {
            if (dbContext != null)
            {
                return dbContext.Orders.First(o=>o.Id==id);
            }

            return null;
        }

        public async Task<DbSet<Order>> GetOrders()
        {


            if (dbContext != null)
            {
                return dbContext.Orders;
            }

            return null;
        }

        public async Task UpdateOrder(Order dto)
        {
            throw new NotImplementedException();
        }
    }
}

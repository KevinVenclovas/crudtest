﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Core.Models;

namespace WebApplication1.Core.Services
{
    public interface IOrderRepository
    {
        Task<DbSet<Order>> GetOrders();
        Task<Order> GetOrder(int id);
        Task UpdateOrder(Order dto);

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebApplication1.Core.DTO;
using WebApplication1.Core.Models;
using WebApplication1.Core.Services;
using WebApplication1.DB;

namespace WebApplication1.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class PrintController : ControllerBase
    {
        private IOrderRepository _orderRepository;
        private IMapper _mapper;
        private MapperConfiguration _mapCfg;

        public PrintController(IOrderRepository orderRepository,IMapper mapper)
        {
            _orderRepository = orderRepository;
            _mapper = mapper;

        }


        [HttpGet]
        public async Task<IActionResult> Print()
        {
            string errormessage = "";
            try
            {
                try
                {
                    var orders = _orderRepository.GetOrders();
                    var model = _mapper.Map<List<OrderDto>>(orders.Result.ToList());

                    return Ok(orders);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex);
                }

            }
            catch (Exception e)
            {
                errormessage = e.Message;
            }
            throw new Exception(errormessage);
        }

    }
}

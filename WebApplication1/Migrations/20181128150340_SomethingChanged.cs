﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication1.Migrations
{
    public partial class SomethingChanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //Hat sich doch nichts geändert, war nur ein Test
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
